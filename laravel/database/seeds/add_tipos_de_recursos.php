<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class add_tipos_de_recursos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tbl_tipos_recursos')->insert([
        'trc_nombre' => 'titulo',
        'trc_tipo' => 'text',
        'trc_activo' => 1,
      ]);
      DB::table('tbl_tipos_recursos')->insert([
        'trc_nombre' => 'bajada',
        'trc_tipo' => 'text',
        'trc_activo' => 1,
      ]);
      DB::table('tbl_tipos_recursos')->insert([
        'trc_nombre' => 'imagen',
        'trc_tipo' => 'imagen',
        'trc_activo' => 1,
      ]);
    }
}
