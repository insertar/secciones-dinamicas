<?php

use Illuminate\Database\Seeder;

class add_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tbl_users')->insert([
        'usr_nombre' => 'Administrador',
        'usr_email' => 'matias.danil@hotmail.com',
        'usr_email_verified_at' => 'matias.danil@hotmail.com',
        'usr_password' => Hash::make('asd'),
      ]);
    }
}
