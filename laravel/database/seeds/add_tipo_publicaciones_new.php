<?php

use Illuminate\Database\Seeder;

class add_tipo_publicaciones_new extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_tipo_publicaciones')->insert([
            'pub_nombre' => 'Categoria',
            'pub_estado' => 1,
        ]);
        DB::table('tbl_tipo_publicaciones')->insert([
            'pub_nombre' => 'Contenido',
            'pub_estado' => 1,
        ]);
        DB::table('tbl_tipo_publicaciones')->insert([
          'pub_nombre' => 'Enlace',
          'pub_estado' => 1,
        ]);
    }
}
