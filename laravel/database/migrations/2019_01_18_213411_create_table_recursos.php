<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRecursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_recursos', function (Blueprint $table) {
            // Codigo interno que identifica al recurso
            $table->increments('rec_codigo');

            // Indica a que seccion corresponde el recurso
            $table->integer('rec_codsec');

            // Si el recurso es parte de un grupo (Ejemplo: galeria principal)
            $table->string('rec_grupo');

            // Entero por el cual se ordenan los recursos
            $table->integer('rec_indice');

            // Identifica el archivo o bien el codigo del video
            $table->string('rec_archivo');

            // Cada recurso le permitimos un titulo
            $table->string('rec_titulo')->nullable();

            // Cada recurso le permitimos una breve descripcion o bajada
            $table->string('rec_descripcion')->nullable();

            // Obtenemos width y eight del recurso
            $table->integer('rec_width')->nullable();

            // Height
            $table->integer('rec_height')->nullable();

            // Guardamos el MIME del recurso
            $table->string('rec_mime')->nullable();

            // Se usa la sesion del usuario para subir recursos a una seccion antes de guardar
            $table->string('rec_session')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_recursos');
    }
}
