<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSecciones extends Migration
{
    /**
     * Run the migrations.
     * Las secciones conforman el contenido del sitio
     * Una seccion se compone de varios elementos, ya que puede ser del tipo categoria o contenido
     *
     * Una seccion puede destacarse en 3 sectores por el momento (Slide, destacados home, menu)
     * Una seccion cuando es destacada en slide o destacados tiene que tener posibilidad de imagen + texto
     * El texto se guarda en esta tabla pero las imagenes son recursos
     *
     * Slide: Habilitamos destacado slide y nos habilita el upload de imagen (Se guarda como recurso) y texto lo guardamos aqui
     *
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_secciones', function (Blueprint $table) {

            // Es el codigo que identifica una seccion
            $table->increments('sec_codigo');

            // Por ahora puede haber 2 tipos (Categorias y Contenido) mas adelante podemos agregar mas
            $table->integer('sec_tipo')->default(1);

            // Entero que indica cual es el padre de esta seccion, seria el "contenida en"
            $table->integer('sec_parent')->default(0);

            // Determina si la seccion es destacada o no, si no es destacado lleva 0, sino lleva el codigo de seccion
            $table->integer('sec_publicado')->default(0);

            // Es el nombre de la seccion, usado normalmente para el menu o breadcump
            $table->string('sec_nombre');

            // Es un opcional con prioridad, si configuran este, este sera el nombre del menu (Usado para el mnu solamente)
            $table->string('sec_mnu_nombre')->nullable();

            // Titulo de la seccion
            $table->string('sec_titulo')->nullable();

            // Texto principal de la seccion
            $table->string('sec_texto_principal')->nullable();

            // Texto secundario de la seccion
            $table->string('sec_texto_secundario')->nullable();

            // Seo titulo
            $table->string('sec_seo_titulo')->nullable();

            // Seo descripcion
            $table->string('sec_seo_descripcion')->nullable();

            // Seo palabras claves
            $table->string('sec_seo_keywords')->nullable();

            // Determina si una seccion se encuentra activa o inactiva
            $table->boolean('sec_activo')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_secciones');
    }
}
