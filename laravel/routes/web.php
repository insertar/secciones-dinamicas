<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();


Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/admin/config', 'ConfigController@index')->name('admin');

Route::get('/admin/pages/{id}', 'AdminController@pages')->name('admin');
Route::get('/admin/category/pages/{id?}', 'AdminController@listContent');
Route::get('/admin/page/new', 'AdminController@create');
Route::post('/admin/pages/new', 'AdminController@store');
Route::get('/admin/pages/{id}/update', 'AdminController@update');

// GALERIA
Route::post('/gallery/image/upload', 'GalleryController@upload');
Route::post('/gallery/image/ordering', 'GalleryController@sort');
Route::post('/gallery/image/{id}/delete', 'GalleryController@delete');

//Catalogo
Route::get('/admin/store/list', 'StoreController@list');
Route::get('/admin/store/create', 'StoreController@create');
Route::post('/admin/store/create', 'StoreController@store');
Route::get('/admin/store/update', 'StoreController@update');
Route::post('/admin/store/update', 'StoreController@save');















// Rutas para la administracion

Route::get('/admin/componentes', function () {
  $images = array("http://lorempixel.com/800/460/people/1", "http://lorempixel.com/800/460/people/2");
  return view('admin.componentes')->with('images', $images)->with('galeria1',array('galery'));
})->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




// Rutas para los registros, se usan normalmente en las tablas
Route::get('admin/registros/{tipo}', 'RegistrosController@getRecsByTipo');

// Ruta para agregar una seccion, debe presentar el formulario
Route::get('admin/seccion/{tipo}/add', 'RegistrosController@create');


// NOVEDADES
Route::get('/admin/novedades', 'NovedadesController@list');
Route::get('/admin/novedades/form', 'NovedadesController@create');
Route::post('/admin/novedades/save', 'NovedadesController@store');
Route::get('/admin/novedades/{id}/edit', 'NovedadesController@edit');
Route::post('/admin/novedades/upd', 'NovedadesController@update');
Route::get('/admin/novedades/{id}/del', 'NovedadesController@delete');



