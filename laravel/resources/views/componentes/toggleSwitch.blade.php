<style>
    .switchBtn {
        position: relative;
        display: inline-block;
        width: 41px;
        height: 17px;
        float: right;
    }
    .switchBtn input {display:none;}
    .slide {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
        padding: 5px;
        color: #fff;
    }
    .slide:before {
        position: absolute;
        content: "";
        height: 15px;
        width: 15px;
        left: 25px;
        bottom: 1px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }
    input:checked + .slide {
        background-color: #8CE196;
        padding-left: 29px;
    }
    input:focus + .slide {
        box-shadow: 0 0 1px #01aeed;
    }
    input:checked + .slide:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
        left: -25px;
    }

    .slide.round {
        border-radius: 34px;
    }
    .slide.round:before {
        border-radius: 50%;
    }
</style>

<label class="switchBtn" id="{{$id}}" >
    <input @if(old("check_".$id) > 1) checked @endif type="checkbox" id="check_{{$id}}" name="check_{{$id}}" onclick="action_toggle('{{$target}}')" value="{{$value}}" >
    <div class="slide round"></div>
</label>
