<div class="row">
    <div class="col-lg-12">
        <div class="box box-info">
            <div class="box-header"><h3>{{ $titulo }}</h3></div>
            <div class="box-body">
                <table id="novedades" class="table table-hover table-condensed">
                    <thead>
                    <tr>
                        <th style="width: 120px;">#</th>
                        <th style="width: 50px;">Codigo</th>
                        <th>Tipo</th>
                        <th style="width: 120px;">Destacado</th>
                        <th style="width: 120px;">Activo</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</div>

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            oTable = $('#novedades').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ url('admin/registros/'.$tipo) }}",
                "columns": [
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'sec_codigo', name: 'nov_codigo'},
                    {data: 'rec_valor_text', name: 'sec_tipo'},
                    {data: 'destacados', name: 'destacados',orderable: false, searchable: false},
                    {data: 'activo', name: 'activo', orderable: false, searchable: false},

                ]
            });
        });

    </script>
@endpush