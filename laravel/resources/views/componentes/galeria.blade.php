<input id="{{ $id }}" type="file" class="file" data-preview-file-type="text" >

<div class="modal fade in" id="modal_{{$id}}" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar datos adicionales</h4>
            </div>
            <div class="modal-body">
                <p>Puede agregar a esta imagen un título y una descripción.</p>
                <div class="form-group">
                    <input type="text" id="{{$id}}_title" class="form-control" placeholder="Titulo">
                </div>
                <div class="form-group">
                    <input type="text" id="{{$id}}_description" class="form-control" placeholder="Descripcion">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="continuar('{{$id}}')">Continuar sin agregar</button>
                <button type="button" class="btn btn-primary" onclick="upload('{{$id}}')">Guardar datos</button>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    var allow_ext = @json($allow_format);
    var $options = {
        theme: 'explorer',
        uploadUrl: "{{url('/gallery/image/upload')}}",
        uploadAsync: false,
        minFileCount: 1,
        maxFileCount: 1,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        overwriteInitial: false,
        initialPreview: ["{!! ( !empty($initialPreview) ? implode('","', $initialPreview) : '') !!}"] ,
        initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewConfig: {!! $config !!},
        uploadExtraData: function () {
            return {
                _token: "{{ csrf_token() }}",
                grupo: "{{$grupo}}",
                update: "{{$update}}",
                title: document.getElementById('{{$id}}_title').value,
                description: document.getElementById('{{$id}}_description').value,
            }
        },
        allowedFileExtensions: allow_ext,
        deleteExtraData: {_token: "{{ csrf_token() }}"},
    };

    $("#{{$id}}").fileinput($options).on('filesorted', function(e, params) {
        $.post('{{url('/gallery/image/ordering')}}', {_token: "{{ csrf_token() }}", params:params.stack});
    }).on('filebatchuploadsuccess', function(event, data, previewId, index) {

        //$("#{{$id}}").fileinput('destroy');

        $("#{{$id}}").fileinput({
            theme: 'explorer',
            uploadUrl: "{{url('/gallery/image/upload')}}",
            uploadAsync: false,
            minFileCount: 1,
            maxFileCount: 100,
            showUpload: false, // hide upload button
            showRemove: false, // hide remove button
            overwriteInitial: false,
            initialPreview: data.response.galeria1 ,
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            initialPreviewConfig: data.response.config,
            uploadExtraData: {
                _token: "{{ csrf_token() }}",
                img_key: "1000",
                img_keywords: "happy, places",
                grupo: "{{$grupo}}",
                update: "{{$update}}"
            },
            allowedFileExtensions: allow_ext,
            deleteExtraData: {_token: "{{ csrf_token() }}"}
        });

    }).on('change', function (event) {
        $('#modal_{{$id}}').modal({ backdrop: 'static', keyboard: false
        });
    });

    $( document ).ready(function() {
        //$("#{{$id}}").fileinput('destroy');
        $("#{{$id}}").fileinput($options);
    });

    function upload(id) {
        $("#"+id).fileinput("upload").fileinput('unlock');
        cerrar_modal(id)
    }
    function continuar(id){
        $("#"+id).fileinput("upload").fileinput('unlock');
        cerrar_modal(id)
    }
    function cerrar_modal(id){
        document.getElementById(id+'_title').value = '';
        document.getElementById(id+'_description').value = '';
        $('#modal_'+id).modal('hide');
    }
</script>
@endpush

