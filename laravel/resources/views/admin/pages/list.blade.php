@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-5">
            <div id="default-tree"></div>
        </div>
        <div class="col-lg-7">
            <div class="box box-info">
                <div class="box-header"><h3>Lista de novedades</h3></div>
                <div class="box-body">
                    <table id="secciones" class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th style="width: 120px;">#</th>
                            <th style="width: 50px;">Codigo</th>
                            <th>Titulo</th>
                            <th style="width: 120px;">Destacado</th>
                            <th style="width: 120px;">Activo</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">


        </div>
    </div>

@endsection

@section('css')
    <link rel="stylesheet" href="{{url('vendor/treeview/bootstrap-treeview.min.css')}}">
@stop

@section('js')
    <script src="{{url('vendor/treeview/bootstrap-treeview.min.js')}}"></script>
    <script>
        /*var myTree = [
            {
                text: "Principal",
                nodes: [
                    {
                        text: "Item 1-1",
                        nodes: [
                            {
                                text: "Item 1-1-1"

                            },
                            {
                                text: "Item 1-1-2"
                            }
                        ]
                    },
                    {
                        text: "Item 1-2"
                    }
                ]
            },
            {
                text: "Item 2",
                href: "www.gooogle.com.ar",
            },
            {
                text: "Item 3"
            }
        ];*/

        var myTree = {!! $tree !!};

        $('#default-tree').treeview({
            data: myTree,
            enableLinks: true,
            showTags: true,
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            oTable = $('#secciones').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ url('/admin/category/pages/'.$id) }}",
                "columns": [
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'sec_codigo', name: 'sec_codigo'},
                    {data: 'sec_nombre', name: 'sec_nombre'},
                    {data: 'destacados', name: 'destacados',orderable: false, searchable: false},
                    {data: 'activo', name: 'activo', orderable: false, searchable: false},

                ]
            });
        });

        /*function destacar(url) {
            $.get(url, function (data) {
                location.reload();
            })
        }

        function loadNovedades(id) {
            oTable = $('#secciones').DataTable({
                destroy: true,
                "processing": true,
                "serverSide": true,
                "ajax": "{{ url('/admin/category/pages/') }}/"+id,
                "columns": [
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'nov_codigo', name: 'nov_codigo'},
                    {data: 'nov_titulo', name: 'nov_titulo'},
                    {data: 'destacados', name: 'destacados',orderable: false, searchable: false},
                    {data: 'activo', name: 'activo', orderable: false, searchable: false},

                ]
            });
        }*/
    </script>
@stop