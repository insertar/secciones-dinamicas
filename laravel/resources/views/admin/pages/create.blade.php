@extends('adminlte::page')

@section('content')
    <form method="post" action="{{url('/admin/pages/new')}}">
        @csrf
        <div class="row">
            <div class="col-lg-8">

                <div class="box">
                    <div class="box-body">
                        <button class="btn btn-primary">Guardar</button>
                        <button class="btn btn-default">Cancelar</button>
                    </div>
                </div>

                @if(!empty($errors->all()))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <ul>
                        @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif


                <!-- Nombre de la seccion -->
                <div class="box box-primary box-solid">
                    <div class="box-header">Creación de una nueva sección</div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Desde aqui podes crear una nueva seccion, comencemos...</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre de la seccion" value="{{ !isset($data) ? $data->sec_nombre : old('nombre', '') }}">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Nombre de menu personalizado -->
                <div class="box box-primary box-solid toggle_mnu" style="display: none">
                    <div class="box-header">Nombre del menu</div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="text" class="form-control" name="nombre_menu" id="nombre_menu" placeholder="Nombre personalizado del menu">
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Imagen de destacado -->
                <div class="box box-primary box-solid toggle_destacado" style="display: none">
                    <div class="box-header">Imagen del destacado</div>
                    <div class="box-body">
                        {!! Gallery::images('Destacado_Home','Imagenes de los destacados','galeria_destacado_home') !!}
                    </div>
                </div>

                <!-- Imagen de Slide -->
                <div class="box box-primary box-solid toggle_slide" style="display: none">
                    <div class="box-header">Imagen del slide</div>
                    <div class="box-body">
                        {!! Gallery::images('Slide_Home','Imagenes del slider','galeria_slide_home') !!}
                    </div>
                </div>

                <!-- Enlace Externo -->
                <div class="box box-primary box-solid tipo_contenido show_3" style="display: none">
                    <div class="box-header">Enlace externo</div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="text" class="form-control" name="enlace" id="enlace" placeholder="Ingresar URL">
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Publicacion -->
                <div class="box box-primary box-solid tipo_contenido show_2" style="display:none;">
                    <div class="box-header">Datos de la publicacion
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">

                        <!-- Datos generales de la publicacion -->
                        <h4>Datos generales</h4>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="text" class="form-control" placeholder="Ingresar titulo de la publicación" name="titulo" id="titulo" value="{{!isset($data) ? $data->sec_titulo : old('titulo', '')}}">
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-lg-12">
                                <input type="text" class="form-control" placeholder="Ingresar bajada de la publicación" name="bajada" id="bajada">
                            </div>
                        </div>
                        <hr>

                        <h4>Contenido principal</h4>
                        <!-- Elementos principales de la publicacion -->
                        <div class="row" style="margin-top: 20px">
                            <div class="col-lg-12">
                                {!! Editor::ckEditor('texto_principal','Texto principal') !!}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-lg-12">
                                {!! Gallery::images('Elgrupoquequiero','Galeria Principal', 'Galeria_Contenido_Principal') !!}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-lg-12">
                                {!! Gallery::videos('Subovideos','Galeria de videos principal', 'Galeria_Videos_Principal') !!}
                            </div>
                        </div>
                        <hr>

                        <h4>Contenido secundario</h4>
                        <!-- Elementos secundarios de la publicacion -->
                        <div class="row" style="margin-top: 20px">
                            <div class="col-lg-12">
                                {!! Editor::ckEditor('texto_secundario') !!}
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px">
                            <div class="col-lg-12">
                                {!! Gallery::images('Elgrupoquequiero','Galeria Secundaria','Galeria_Contenido_Secundario') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                {!! Gallery::videos('Subovideos','Galeria de videos secundaria','Galeria_Video_Secundario') !!}
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!-- Panel de configuraciones -->
            <div class="col-lg-4">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h4>Panel de configuraciones</h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Tipo de publicacion: </p>
                            </div>
                            <div class="col-lg-6">
                                <select class="form-control" name="tipo_publicacion" onchange="tipoPublicacion(this.value)">
                                    @foreach($publicaciones as $publicacion)
                                        <option @if(old('tipo_publicacion') == $publicacion->pub_codigo) selected @endif value="{{$publicacion->pub_codigo}}">{{$publicacion->pub_nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Activar</p>
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('activar','Estado','',1) !!}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Contenida en: </p>
                            </div>
                            <div class="col-lg-6">
                                <select class="form-control" id="contenida_en" name="contenida_en">
                                    <option value="0">Categoria Principal</option>
                                    @foreach($categorias as $categoria)
                                        <option value="{{$categoria->sec_codigo}}">{{$categoria->sec_nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Destacar en slide</p>
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('slide','Slide','toggle_slide',1) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Destacar en HomePage</p>
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('destacado','Destacado','toggle_destacado',2) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Destacar en menu</p>
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('menu','Slide','toggle_mnu',4) !!}
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>

                <!-- SEO -->
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h4>Opciones SEO</h4>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Titulo: </p>
                                <input class="form-control" type="text" id="seo_titulo" name="seo_titulo">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Palabras claves: </p>
                                <input class="form-control" type="text" id="seo_key" name="seo_key">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>Descripción: </p>
                                <textarea class="form-control" name="seo_descripcion" id="seo_descripcion"></textarea>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('css')
    <!-- File input -->
    <link rel="stylesheet" href="{{ asset("vendor/file_uploader/css/fileinput.min.css") }}">
    <link rel="stylesheet" href="{{ asset("vendor/file_uploader/themes/explorer/theme.min.css") }}">
@endsection

@section('js')
    <!-- File input -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.1/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="{{ asset("vendor/file_uploader/js/fileinput.min.js") }}"></script>
    <script src="{{ asset("vendor/file_uploader/themes/explorer/theme.min.js") }}"></script>

    <!-- Editor -->
    <script src="{{asset("vendor/ckeditor/ckeditor.js")}}"></script>

    <!-- Toggle -->
    <script>
        function action_toggle(e) {
            $( "."+e).toggle();
        }


        function tipoPublicacion(e) {
            $('.tipo_contenido').hide()
            $('.show_'+e).show()
        }
    </script>
@endsection