@extends('adminlte::page')


@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>Este modulo muestra ejemplos de recursos (Componentes) disponibles para la utilizacion en modulos</p>
    <!--
        Agregando este componente se agrega el editor de texto
        Un editor se puede usar para carga de contenidos o bien para actualizar contenidos
        En el caso de la actualizacion, puede que tengamos un valor que suministrarle
        Ese valor viene desde el controlador en una variable
        esa varible la tenemos que trasladar como valor
    -->
    @component('componentes.editor', ['value' => 'Reemplazar este texto por la variable que contiene el texto del editor'])
        @slot('id', 'editor1')
        @slot('name','matias')
    @endcomponent

    <!--
        Agregando este componente se agrega la galeria de imagenes
        Una Galeria puede ser para cargar nuevas imagenes o actualizar
        En caso de actualizar necesitamos las imagenes que vienen por
        controlador y las pasamos al componente
    -->
    @component('componentes.galeria', ['initialPreview' => $galeria1, 'grupo' =>  'novedadesGallery1'])
        @slot('id', 'galeria1')
    @endcomponent
@stop


