@extends('adminlte::page')


@section('title', 'Nueva seccion')


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header"><p><b>Detalles de seccion</b></p></div>
                <div class="box-body">
                    <p>Tipo de seccion: {{$tipo}} (Recordar consultar el tipo para traer el nombre)</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="box">
                <div class="box-header">
                    Estructura de seccion
                </div>
                <div class="box-body">
                    <form method="post" name="new_seccion" id="new_seccion">
                        <input type="hidden" name="tipo_seccion" id="tipo_seccion" value="{{$tipo}}">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="box">
                <div class="box-header"><b>Recursos disponibles</b></div>
                <div class="box-body">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-plus"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"><b>Texto simple</b></span>
                            <p>Permite el ingreso de texto simple.</span>
                        </div>
                    </div>
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-plus"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"><b>Texto ampliado</b></span>
                            <p>Permite el ingreso de texto ampliado con editor de texto.</span>
                        </div>
                    </div>
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-plus"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text"><b>Galeria de imagenes</b></span>
                            <p>Permite la carga de imagenes.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

