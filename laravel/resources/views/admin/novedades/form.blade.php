@extends('adminlte::page')


@section('title', 'Dashboard')


@section('content')
    <form method="post" action="{{$data['url']}}">
        @csrf
    @component('componentes.guardar_cancelar')
    @endcomponent

    @if(isset($data['codsec']) && $data['codsec'])
        <input type="hidden" name="id" id="id" value="{{$data['codsec']}}">
    @endif
    <div class="row">
        <div class="col-lg-8">
            <div class="box">
                <div class="box-header">
                    <p><b>Formulario ({{$data['titulo']}})</b></p>
                </div>
                <div class="box-body">

                    @component('componentes.text', ['label' => 'Titulo', 'name'=>'titulo', 'id' => 'titulo', 'value'=>$recurso['titulo']])
                    @endcomponent

                    @component('componentes.text', ['label' => 'Bajada', 'name'=>'bajada', 'id' => 'bajada', 'value'=>$recurso['bajada']])
                    @endcomponent

                    @component('componentes.editor', ['id' => 'texto1', 'name'=>'texto1', 'value'=>$recurso['texto1']])
                    @endcomponent

                    @component('componentes.galeria', ['id' => 'galeria1', 'initialPreview' => $recurso['galeria1']['preview'], 'grupo' => 'novedadesGallery1', 'config' => $recurso['galeria1']['config'], 'update' => $data['codsec']]);
                    @endcomponent

                </div>
            </div>
        </div>
    </div>
    </form>
@stop

