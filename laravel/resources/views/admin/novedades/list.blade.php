@extends('adminlte::page')


@section('title', 'Dashboard')

@section('content_header')
    <h1>Novedades</h1>
@stop

@section('content')
    @component('componentes.total_add', ['titulo'=>'Novedades Totales', 'cantidad'=>$cantidad, 'url'=>'admin/novedades/form'])
    @endcomponent

    @component('componentes.list', ['titulo'=>'Listado de novedades', 'tipo'=>\App\Library\tipoSecciones::NOVEDADES])
    @endcomponent
@stop

