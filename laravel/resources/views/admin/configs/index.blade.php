@extends('adminlte::page')

@section('content')

    <div class="row">
        <div class="col-lg-8">
            <!-- BOX Generales -->
            <div class="box box-default">
                <div class="box-header">
                    <h4>Configuraciones generales</h4>
                </div>
                <div class="box-body">

                    <!-- Titulo del sitio -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Titulo del sitio
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="titulo" id="titulo">
                            </div>
                        </div>
                    </div>

                    <!-- Logo del sitio -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Logotipo del sitio
                            </div>
                            <div class="col-lg-6">
                                {!! Upload_Image::show() !!}
                            </div>
                        </div>
                    </div>

                    <!-- URL Del sitio -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                URL del sitio
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="url" id="url">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- BOX redes -->
            <div class="box box-default">
                <div class="box-header">
                    <h4>Redes sociales</h4>
                </div>
                <div class="box-body">

                    <!-- Facebook -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Facebook
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="faecbook" id="facebook">
                            </div>
                        </div>
                    </div>

                    <!-- Twitter -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Twitter
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="twitter" id="twitter">
                            </div>
                        </div>
                    </div>

                    <!-- Linkedin -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Linkedin
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="linkedin" id="linkedin">
                            </div>
                        </div>
                    </div>

                    <!-- Youtube -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Youtube
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="youtube" id="youtube">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- BOX Contacto -->
            <div class="box box-default">
                <div class="box-header">
                    <h4>Configuraciones de contacto</h4>
                </div>
                <div class="box-body">
                    <!-- Telefono -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Telefono
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="telefono" id="telefono">
                            </div>
                        </div>
                    </div>

                    <!-- Direccion -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Direccion
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="direccion" id="direccion">
                            </div>
                        </div>
                    </div>

                    <!-- E-mail -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                E-mail
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="email" id="email">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <!-- Mostrar campo Nombre -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Campo nombre
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('contacto_nombre', 'este es un texto',$target='',$value=0) !!}
                            </div>
                        </div>
                    </div>

                    <!-- Mostrar campo Asunto -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Campo Asunto
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('contacto_asunto', 'este es un texto',$target='',$value=0) !!}
                            </div>
                        </div>
                    </div>

                    <!-- Mostrar campo Direccion -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Campo Direccion
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('contacto_direccion', 'este es un texto',$target='',$value=0) !!}
                            </div>
                        </div>
                    </div>

                    <!-- Mostrar campo E-mail -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Campo E-mail
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('contacto_email', 'este es un texto',$target='',$value=0) !!}
                            </div>
                        </div>
                    </div>

                    <!-- Mostrar campo Telefono -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Campo Telefono
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('contacto_telefono', 'este es un texto',$target='',$value=0) !!}
                            </div>
                        </div>
                    </div>

                    <hr>

                    <!-- Mail recipiente -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Recipiente
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="recipiente" id="recipiente">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- BOX Google -->
            <div class="box box-default">
                <div class="box-header">
                    <h4>Configuraciones de google</h4>
                </div>
                <div class="box-body">
                    <!-- Codigo maps -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Codigo google maps
                            </div>
                            <div class="col-lg-6">
                                <textarea class="form-control" name="maps" id="maps"></textarea>
                            </div>
                        </div>
                    </div>

                    <!-- Google Analitics -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Codigo Analitics
                            </div>
                            <div class="col-lg-6">
                                <textarea class="form-control" name="analitics" id="analitics"></textarea>
                            </div>
                        </div>
                    </div>

                    <!-- Recaptcha KEY -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Captcha Key
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="captcha_key" id="captcha_key">
                            </div>
                        </div>
                    </div>

                    <!-- Recaptcha Secret -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Captcha Secret
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="captcha_secret" id="captcha_secret">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- BOX SEO -->
            <div class="box box-default">
                <div class="box-header">
                    <h4>Configuraciones SEO generales</h4>
                </div>
                <div class="box-body">


                    <!-- Titulo -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Titulo
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="titulo" id="titulo">
                            </div>
                        </div>
                    </div>

                    <!-- Descripcion -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Descripcion
                            </div>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="descripcion" name="descripcion"></textarea>
                            </div>
                        </div>
                    </div>

                    <!-- KeyWords -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Palabras claves
                            </div>
                            <div class="col-lg-6">
                                <textarea class="form-control" id="keywords" name="keywords"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-lg-4">
            <div class="box">
                <div class="box-header">
                    Configuraciones adicionales
                </div>
                <div class="box-body">
                    <!-- Sitio en mantenimiento -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                Sitio en mantenimiento
                            </div>
                            <div class="col-lg-6">
                                {!! ToggleSwitch::rounded('contacto_nombre', 'este es un texto',$target='',$value=0) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- File input -->
    <link rel="stylesheet" href="{{ asset("vendor/file_uploader/css/fileinput.min.css") }}">
    <link rel="stylesheet" href="{{ asset("vendor/file_uploader/themes/explorer/theme.min.css") }}">
@endsection

@section('js')
    <!-- File input -->
    <script src="{{ asset("vendor/file_uploader/js/fileinput.min.js") }}"></script>
    <script src="{{ asset("vendor/file_uploader/themes/explorer/theme.min.js") }}"></script>
@endsection