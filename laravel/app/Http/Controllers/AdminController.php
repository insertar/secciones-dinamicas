<?php

namespace App\Http\Controllers;

use App\Seccione;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Recurso;
use App\TipoPublicacion;
use Validator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller{

  private $errors = array();
  private $tree = array();

  public function __construct(){ }

  public function index(){
    return view('admin.index');
  }

  /** Muestra listado de categorias y las pages que corresponden a cada categoria */
  public function pages($id){
    // Arma el arbol
    $this->getTree($this->tree,0,$id);

    return view('admin.pages.list')->with('id',$id)->with('tree',json_encode($this->tree));
  }

  public function listContent($id){

    //DB::enableQueryLog();
    $secciones = Seccione::where('sec_parent',$id)->whereIn('sec_tipo',[2,3])->get();
    //dd(DB::getQueryLog());

    return Datatables::of($secciones)
      ->addColumn('action', function ($secciones) {
        return '<a href="'.url('/novedades/edit/'.$secciones->sec_codigo).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                   <a href="'. url('novedades/delete', $secciones->sec_codigo) .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>   ';
      })
      ->addColumn('destacados', function ($secciones) {
        return '<div class="checkbox checkbox-slider--b-flat pull-left">
                                <label>
                                    <input onclick="destacar(\''. url('/admin/novedades/'.$secciones->sec_codigo.'/destacar') .'\')" name="destacado" type="checkbox"  '. (isset($secciones->sec_destacado) && $secciones->sec_destacado ? "checked" : "" ) .'  ><span></span>
                                </label>
                            </div>';

      })
      ->addColumn('activo', function ($secciones) {
        return '<div class="checkbox checkbox-slider--b-flat pull-left">
                                <label>
                                    <input onclick="destacar(\''. url('/admin/novedades/'.$secciones->sec_codigo.'/activar') .'\')" name="destacado" type="checkbox"  '. (isset($secciones->sec_activo) && $secciones->sec_activo ? "checked" : "" ) .'  ><span></span>
                                </label>
                            </div>';

      })
      ->rawColumns(['destacados', 'activo','action'])
      ->make(true);
  }


  public function getTree(&$tree,$parent,$id) {

    $secciones = Seccione::where('sec_parent', $parent)->where('sec_tipo', 1)->get();


      foreach ($secciones as $key => $seccion) {
        // Obtengo la cantidad de registros por seccion para mostrar en el label
        $cantidad = Seccione::where('sec_parent', $seccion->sec_codigo)->whereIn('sec_tipo',[2,3])->count();

        $selected = false;
        if($seccion->sec_codigo == $id){
          $selected = true;
        }

        $tree[]  = array( 'text'=>$seccion->sec_nombre,
                          'href'=>url('admin/pages/'.$seccion->sec_codigo),
                          'state' => array('selected' => $selected, 'expanded' => true),
                          'tags' => array($cantidad),
                          'selectable' => false
                        );


        $this->getTree($tree[$key]['nodes'],$seccion->sec_codigo,$id);

      }


  }


  public function create(Request $request){
      $publicaciones = TipoPublicacion::where('pub_estado',1)->get();
      $categorias = Seccione::where('sec_tipo', 1)->get();

      return view('admin.pages.create')
        ->with('publicaciones', $publicaciones)
        ->with('categorias',$categorias)
        ->with('data','');
  }

  public function store(Request $request){

    // Antes de realizar las validaciones, necesito saber el tipo de publicacion
    $tipo_publicacion = $request->input('tipo_publicacion');

    // No permitir ningun tipo de publicacion que no sean las definidas en base de datos
    $tipos = TipoPublicacion::where('pub_estado', 1)->where('pub_codigo', $tipo_publicacion)->first();
    if(empty($tipos->pub_codigo)){
      $this->errors[] = 'El tipo de publicacion que intenta registrar no existe.';
    }

    // Validamos que el nombre de la seccion no este vacio, es obligatorio
    if(empty($request->input('nombre'))){
      $this->errors[] = 'Debe asignar un nombre a la sección';
    }

    // No se valida mas nada, lo demas es opcion en tal caso sale vacio el conteido pero lo limito por front.

    // Ahora validar los formatos de cada campo.
    $validator = Validator::make($request->all(), [
      'nombre' => 'required',
      'tipo_publicacion' => 'required|numeric',
      'check_activar' => 'boolean',
      'contenida_en' => 'numeric',
      'check_slide' => 'numeric',
      'check_destacado' => 'numeric',
      'check_menu' => 'numeric'
    ]);

    if ($validator->fails()) {
      return redirect('admin/pages/new')
        ->withErrors($validator)
        ->withInput();
    }

    $publicar = $request->input('check_slide') + $request->input('check_destacado') + $request->input('check_menu');

    $save = new Seccione();
    $save->sec_tipo = $request->input('tipo_publicacion');
    $save->sec_parent = $request->input('contenida_en');
    $save->sec_publicado =  $publicar;
    $save->sec_nombre = $request->input('nombre');
    $save->sec_mnu_nombre = $request->input('nombre_menu');
    $save->sec_titulo = $request->input('titulo');
    $save->sec_texto_principal = $request->input('texto_principal');
    $save->sec_texto_secundario = $request->input('texto_secundario');
    $save->sec_seo_titulo = $request->input('seo_titulo');
    $save->sec_seo_descripcion = $request->input('seo_descripcion');
    $save->sec_seo_keywords = $request->input('seo_key');
    $save->sec_activo = !empty($request->input('check_activar')) ? $request->input('check_activar') : 0;
    $save->save();

    // Una vez grabada la seccion hay que vincular los recursos
    // Buscamos todos los recursos que tienen la session activa y les actualizamos el codigo de seccion
    Recurso::where('rec_session',$request->session()->getId())
      ->update(array('rec_session' => '', 'rec_codsec' => $save->sec_codigo));

    // Redireccionar a la pantalla de actualizacion
    return redirect('admin/pages/'.$save->sec_codigo.'/update');


  }

  public function update($id){
    // Recuperar registro
    $seccion = Seccione::where('sec_codigo', $id)->first();
    $recursos = Recurso::where('rec_codsec', $seccion->sec_codigo)->get();

    $publicaciones = TipoPublicacion::where('pub_estado',1)->get();
    $categorias = Seccione::where('sec_tipo', 1)->get();

    return view('admin.pages.create')
      ->with('publicaciones', $publicaciones)
      ->with('categorias',$categorias)
      ->with('data',$seccion);
  }

}
