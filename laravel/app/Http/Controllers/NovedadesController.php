<?php

namespace App\Http\Controllers;

use App\Library\tipoSecciones;
use Illuminate\Http\Request;
use App\Seccione;
use App\Recurso;
use App\TipoRecurso;

class NovedadesController extends Controller{

  // -------------------------------------------------------------------------------------------------------------------
  // LISTADOS ----------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------
  public function list(){
    $cantidad = Seccione::where('sec_tipo', tipoSecciones::NOVEDADES)->get();
    return view('admin.novedades.list')
      ->with('tipo', tipoSecciones::NOVEDADES)
      ->with('cantidad', count($cantidad));
  }


  // -------------------------------------------------------------------------------------------------------------------
  // AGREGAR ----------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------
  /** Todos los metodos CREATE deben usarse para mostrar los formularios
   *  Ojo con las variables que para el create no se usan, pero si para el update.
   */
  public function create(Request $request){

    // Obtengo las imagenes de la session
    $recs = Recurso::where('rec_session', $request->session()->getId())->orderby('rec_indice', 'ASC')->get();
    $galeria1 = array();
    $config = array();
    // Recorro las imagenes y genero los elementos para el plugin
    // Galeria1 contiene la URL completa a la imagen, se usa para el preview
    // Config se usa para elementos del plugin, ejemplo, usa URL como la URL del icono de delete
    // tambien usa caption para el nombre visible de la imagen y Key que es el numero de codigo de la imagen que creo
    // no se esta usando
    foreach($recs as $rec){
      $galeria1[] = asset('storage/uploads/'.$rec->rec_valor_text);
      $config[] = array('key'=>$rec->rec_codigo,'caption' => $rec->rec_valor_text, 'url' => asset('admin/recursos/'.$rec->rec_codigo.'/delete'));
    }

    $recursos['titulo'] = '';
    $recursos['bajada'] = '';
    $recursos['texto1'] = '';
    $recursos['galeria1']['preview'] = $galeria1;
    $recursos['galeria1']['config'] = json_encode($config);


    $data['titulo'] = 'Crear';
    $data['url'] = url('admin/novedades/save');
    $data['codsec'] = false;
    return view('admin.novedades.form')
      ->with('recurso', $recursos)
      ->with('data', $data);
  }
  public function store(Request $request){
    $datos = $request->all();

    // Primero hay que guardar la novedad para obtener el codigo
    $seccion = new Seccione();
    $seccion->sec_tipo = tipoSecciones::NOVEDADES;
    $seccion->sec_destacado = 0;
    $seccion->sec_activo = 1;
    $seccion->save();

    // Detalle de un recurso
    // Un recurso puede ser un texto, un enlace, un titulo, una bajada, etc
    // todos los recursos vienen de un solo campo rec_valor_text
    // Por ejemplo, si me agregan 2 textos descriptivos tengo que agregar 2 recursos para esa seccion
    // o sea, 2 recursos del tipo texto para la misma seccion
    // El tema es que para poder identificar cada uno, tendria que asignarle un grupo o identificarlo de alguna manera
    // Se me ocurre lo siguiente, despues mas adelante vemos
    // El grupo que sea el name y el tipo por ahora no lo necesito

    // Una vez guardado tengo que guardar los recursos
    // Recorro los recursos pero evito el token
    // Todos los input deben tener el identificador un guin bajo y el numero

    foreach ($datos as $key => $dato){
      if($key == '_token'){ continue; }

      $recurso = new Recurso();
      $recurso->rec_codsec = $seccion->sec_codigo;
      $recurso->rec_grupo = $key;

      $recurso->rec_tipo = '';
      $recurso->rec_indice = 0;
      $recurso->rec_valor_text = $dato;
      $recurso->rec_session = '';
      $recurso->save();

      // Por ultimo, tengo que buscar todas las imagenes que esten con session, quitarle la session y asignarle
      // el codigo de seccion
      Recurso::where(['rec_session' => $request->session()->getId()])->update(['rec_session' => '', 'rec_codsec' => $seccion->sec_codigo]);

    }

    return redirect('/admin/novedades/'.$seccion->sec_codigo.'/edit');
  }

  // -------------------------------------------------------------------------------------------------------------------
  // EDITAR ----------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------
  public function edit($codsec){
    // Buscamos la novedad y los recursos correspondientes a la edicion
    $rec = Seccione::where('sec_codigo', $codsec)->leftJoin('tbl_recursos', 'sec_codigo', '=', 'rec_codsec')->orderby('rec_indice', 'ASC')->get();

    $galeria1 = array();
    $config = array();
    foreach ($rec as $item) {
      if($item->rec_tipo == 'image'){
        $galeria1[] = asset('storage/uploads/'.$item->rec_valor_text);
        $config[] = array('key'=>$item->rec_codigo,'caption' => $item->rec_valor_text, 'url' => asset('admin/recursos/'.$item->rec_codigo.'/delete'));
      }

      $recursos[$item->rec_grupo] = $item->rec_valor_text;
      $recursos['galeria1']['preview'] = $galeria1;
      $recursos['galeria1']['config'] = json_encode($config);
    }

    $data['titulo'] = 'Actualizar';
    $data['url'] = url('admin/novedades/upd');
    $data['codsec'] = $codsec;
    return view('admin.novedades.form')->with('data', $data)->with('recurso', $recursos);
  }
  public function update(Request $request){
    // Lo que tengo que actualizar son los recursos, las seccion solo se actualiza el destacado y el activo

    // Actualizando los recursos

    $id = $request->input('id');

    $datos = $request->all();

    foreach ($datos as $key => $dato){
      if($key == '_token'){ continue; }
      if($key == 'id'){ continue; }

      Recurso::where('rec_codsec', $id)->where('rec_grupo', $key)->update(['rec_valor_text' => $dato]);
    }

    return redirect('/admin/novedades/'.$id.'/edit');

  }

  // -------------------------------------------------------------------------------------------------------------------
  // Eliminar ----------------------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------------------------------
  public function delete($id){

    $recs = Recurso::where('rec_codsec', $id)->get();
    foreach ($recs as $rec){
      $image_path = 'public/uploads/'.$rec->rec_value_text;
      $rec->delete();
      if($rec->rec_tipo == 'image'){
        Storage::delete($image_path);
      }
    }

    Seccione::where('sec_codigo', $id)->delete();

    return redirect('/admin/novedades');
  }



}
