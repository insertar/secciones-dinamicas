<?php

/** Un recurso es un elemento que subimos en algun modulo, tenemos diferentes tipos de recursos
 *  Por ahora solo vamos a tratar imagen
 *  Al subir un recurso guardamos
 *  rec_codigo: Identifica el recurso en cuestion
 *  rec_codsec: Es la seccion a la cual esta vinculado el recurso (Ejemplo, codigo de una novedad)
 *  rec_tipo: Es el tipo de recurso y tiene relacion con la tabla tbl_tipos_recursos
 *  rec_indice: es el indice del recurso para ordenamientos
 *  rec_parent: marca si el recurso es padre o tiene hijos.
 *  rec_valor_simple: Por si el recurso es simplemente una imagen por ejemplo
 *  rec_valor_text: en caso de recursos que tengan texto
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recurso;
use Image;
use Illuminate\Support\Facades\Storage;

class RecursosController extends Controller{



}
