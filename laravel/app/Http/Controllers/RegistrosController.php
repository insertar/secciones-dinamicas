<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seccione;
use Yajra\Datatables\Datatables;

class RegistrosController extends Controller{

  public function getRecsByTipo($tipo){
    $secciones = Seccione::where('sec_tipo', $tipo)->where('rec_grupo', 'titulo')->leftJoin('tbl_recursos', 'sec_codigo', '=', 'rec_codsec')->get();

    return Datatables::of($secciones)
      ->addColumn('action', function ($secciones) {
        return '<a href="'.url('/admin/novedades/'.$secciones->sec_codigo.'/edit').'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                   <a href="'. url('/admin/novedades/'.$secciones->sec_codigo.'/del') .'" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</a>   ';
      })
      ->addColumn('destacados', function ($secciones) {
        return '<div class="checkbox checkbox-slider--b-flat pull-left">
                                <label>
                                    <input onclick="destacar(\''. url('/admin/novedades/'.$secciones->sec_codigo.'/destacar') .'\')" name="destacado" type="checkbox"  '. (isset($secciones->sec_destacado) && $secciones->sec_destacado ? "checked" : "" ) .'  ><span></span>
                                </label>
                            </div>';
      })
      ->addColumn('activo', function ($secciones) {
        return '<div class="checkbox checkbox-slider--b-flat pull-left">
                                <label>
                                    <input onclick="destacar(\''. url('/admin/novedades/'.$secciones->sec_codigo.'/activar') .'\')" name="destacado" type="checkbox"  '. (isset($secciones->sec_activo) && $secciones->sec_activo ? "checked" : "" ) .'  ><span></span>
                                </label>
                            </div>';
      })
      ->rawColumns(['destacados', 'activo','action'])
      ->make(true);
  }

  public function create($tipo){
    /** Supongamos que viene el tipo de seccion que quiero crear, en ejemplo 1 que seria novedades
     *  Tendria que presentar un formulario pero de novedades, podria tener un formulario generico
     *  que puedan agregar elementos como lo habia definido en las notas
     *  pero le pasamos como campo hidden el tipo de seccion que seria
     *  entonces, si es una novedad, permitira agragar los elementos y ya sabria que es novedad, a ver intentemos
     *
     */
    return view('admin.secciones.form')->with('tipo', $tipo);
  }
}
