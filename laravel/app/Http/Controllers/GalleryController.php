<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recurso;
use Image;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller{

    ## GALERIA DE IMAGENES
    ######################################################################################################################

    /**
     * Realiza el upload de la imagen y la guarda en la base de datos
     */
    public function upload(Request $request){
        $file = $request->file('file_data');

        try{
            $mime = $file->getMimeType();
            $fileType = explode('/', $file->getMimeType());
        }catch(\Exception $e){
            $mime = $file->getClientMimeType();
            $fileType = explode('/', $file->getClientMimeType());
        }





        $update = $request->input('update');

        // El nombre queda definido por el mimetype
        if ($fileType[0] == 'image') {
            $filename = substr($fileType[0], 0, 1).round(microtime(true) * 1000).'.'.$fileType[1];
        } else {
            $filename = substr($fileType[0], 0, 1).round(microtime(true) * 1000).'.'.$file->getClientOriginalExtension();
        }


        try{
            $request->file('file_data')->storeAs('public/uploads', $filename);
        }catch (\Exception $e){
            echo 'El archivo es mayor a 2mb<br>';
            die();
        }



        list($width, $height) = getimagesize($file);

        // Grabamos el recurso en la tabla
        $rec = new Recurso();
        // Codigo de seccion va en 0, no conocemos el mismo, lo actualizamos cuando actualizamos la page
        $rec->rec_codsec = !$update ? 0 : $update;

        //$ Grabamos el grupo al que pertenece la  imagen}
        $rec->rec_grupo = $request->input('grupo');

        // Ver como envia los datos para guadar el indice.
        $rec->rec_indice = 0;

        // Ver como pongo el titulo y la descripcion por ahora van vacios
        $rec->rec_width = $width;
        $rec->rec_height = $height;
        $rec->rec_mime = $mime;
        $rec->rec_titulo = $request->input('title');
        $rec->rec_descripcion = $request->input('description');;

        // Es una imagen por lo que su value es simple
        $rec->rec_archivo = $filename;

        // Un upload puede venir de una creacion o de un update, en el caso de que sea una creacion
        // la session se almacena, sino no
        $rec->rec_session = !$update ? $request->session()->getId() : '';
        $rec->save();

        if(!$update){
            $recs = Recurso::where('rec_session', $request->session()->getId())->get();
        }else{
            $recs = Recurso::where('rec_codsec', $update)->where('rec_grupo', $request->input('grupo'))->get();
        }

        $galeria1 = array();
        foreach($recs as $rec){
            $galeria1[] = asset('storage/uploads/'.$rec->rec_archivo);
            $config[] = array('key'=>$rec->rec_codigo,'caption' => $rec->rec_archivo, 'url' => asset('gallery/image/'.$rec->rec_codigo.'/delete'));

        }

        return array('config' => $config, 'galeria1' => $galeria1);
    }

    /**
     * Este metodo sirve para limpiar la basura subida no utilizada
     * Tiene que estar presente siempre que accedemos a un modulo que permite upload de imagenes
     * y tiene que ser lo primero que se ejecuta para limpiar cualquier basura que podamos tener
     * de algun otro modulo que queriamos subir una imagen y nos arrepentimos y cerramos la ventana directamente.
     * !! Falta acomodar este modulo, lo copie de ferroni y aca no va a andar
     */
    public function limpiar_basura(){
        $basura = Resource::where([ ['rec_session', '<>', ''] , ['created_at', '<', date('Y-m-d 00:00:00')]])->get();
        foreach ($basura as $archivo){
            $rec = Resource::where('rec_codigo', $archivo->rec_codigo)->first();
            $image_path = 'public/uploads/'.$rec->rec_archivo;
            $rec->delete();
            Storage::delete($image_path);
        }
    }

    /** Ordena las imagenes al moverlas */
    public function sort(Request $request){
        $images = $request->input('params');

        foreach ($images as $key => $data){
            Recurso::where('rec_codigo', $data['key'])->update(['rec_indice' => $key]);
        }
    }

    /**
     * Elimina una imagen
     */
    public function delete($id){
        $rec = Recurso::where('rec_codigo', $id)->first();
        $image_path = 'public/uploads/'.$rec->rec_archivo;
        $rec->delete();
        Storage::delete($image_path);
        return json_encode(array());
    }

    /** Metodo que elimina todos los recursos que pertenecen a una seccion */
    public function deleteByCodSec($id){
        // Obtengo todos los recursos de una seccion
        $recs = Recurso::where('rec_codsec', $id)->get();
        foreach ($recs as $rec){
            $image_path = 'public/uploads/'.$rec->rec_archivo;
            $rec->delete();
            if($rec->rec_tipo == 'image'){
                Storage::delete($image_path);
            }
        }

        //return view('admin/novedades/edit/'.$codrec);
    }

}
