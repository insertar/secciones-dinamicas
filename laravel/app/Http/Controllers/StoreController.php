<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function list(){
        return view('admin.store.products.form');
    }

    public function create(){
        return view('admin.store.products.form');
    }

    public function store(){
        return view('admin.store.products.form');
    }

    public function update(){
        return view('admin.store.products.form');
    }

    public function save(){
        return view('admin.store.products.form');
    }
}
