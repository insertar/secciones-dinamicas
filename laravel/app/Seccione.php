<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seccione extends Model
{
  public $table = "tbl_secciones";
  protected $fillable = ['sec_tipo','sec_activo'];
  protected $primaryKey = 'sec_codigo';
}
