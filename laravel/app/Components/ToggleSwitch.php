<?php

namespace App\Components;

use App\Recurso;
use Illuminate\Support\Facades\URL;

class ToggleSwitch{

  public function rounded($id, $text,$target='',$value=0){
    $element = view('componentes.toggleSwitch')
      ->with('id',$id)
      ->with('text',$text)
      ->with('target',$target)
      ->with('value',$value);
    return $element;
  }

}