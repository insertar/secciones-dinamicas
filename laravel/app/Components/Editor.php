<?php

namespace App\Components;

use App\Recurso;
use Illuminate\Support\Facades\URL;

class Editor{

    public function ckEditor($id, $placeholder=''){
        $galeria = view('componentes.editor')->with('id',$id)->with('placeholder',$placeholder);
        return $galeria;
    }

}