<?php

namespace App\Components;

use App\Recurso;
use Illuminate\Support\Facades\URL;

class Gallery{

    /**
     * $group = grupo al que pertenece la imagen
     * $id = Es el id que identificara el file input
     */
    public function images($group, $title, $id){
        // Obtenemos la session para guardar las imagenes en un primer upload de algun contenido
        $session = session()->getId();

        // Recupero las imagenes que estan en session
        $recs = Recurso::where('rec_session', $session)->where('rec_mime','image/png')->orderby('rec_indice', 'ASC')->get();

        // Armo la configuracion para los preview
        $galeria1 = array();
        $config = array();
        foreach($recs as $rec){
            $galeria1[] = asset('storage/uploads/'.$rec->rec_archivo);
            $config[] = array('key'=>$rec->rec_codigo,'caption' => $rec->rec_archivo, 'url' => asset('gallery/image/'.$rec->rec_codigo.'/delete'));
        }

        // Genero la vista ya armada
        $galeria = view('componentes.galeria')
            ->with('title',$title)
            ->with('id',$id)
            ->with('initialPreview',$galeria1)
            ->with('config',json_encode($config))
            ->with('grupo',$group)
            ->with('allow_format', ['jpg','png','gif'])
            ->with('update','');

        // Retorno el componente
        return $galeria;
    }

    public function videos($group, $title){
        // Obtenemos la session para guardar las imagenes en un primer upload de algun contenido
        $session = session()->getId();

        // Recupero las imagenes que estan en session
        $recs = Recurso::where('rec_session', $session)->where('rec_mime','video/mp4')->orderby('rec_indice', 'ASC')->get();

        // Armo la configuracion para los preview
        $galeria1 = array();
        $config = array();
        foreach($recs as $rec){
            $galeria1[] = asset('storage/uploads/'.$rec->rec_archivo);
            $type = explode('/',$rec->rec_mime);
            $config[] = array('filename'=>$rec->rec_archivo,'type'=>$type[0],'filetype'=>$rec->rec_mime,'key'=>$rec->rec_codigo,'caption' => $rec->rec_archivo, 'url' => asset('gallery/image/'.$rec->rec_codigo.'/delete'));
        }

        // Genero la vista ya armada
        $galeria = view('componentes.galeria')
            ->with('title',$title)
            ->with('id','galeria2')
            ->with('initialPreview',$galeria1)
            ->with('config',json_encode($config))
            ->with('grupo',$group)
            ->with('allow_format', ['avi','mpeg','mp4'])
            ->with('update','');

        // Retorno el componente
        return $galeria;
    }

}