<?php

namespace App\Components;

use App\Recurso;
use Illuminate\Support\Facades\URL;

class Upload_Image{

    /**
     * $group = grupo al que pertenece la imagen
     * $id = Es el id que identificara el file input
     */
    public function show(){
        // Obtenemos la session para guardar las imagenes en un primer upload de algun contenido
        //$session = session()->getId();

        // Recupero las imagenes que estan en session
        //$recs = Recurso::where('rec_session', $session)->where('rec_mime','image/png')->orderby('rec_indice', 'ASC')->get();

        // Armo la configuracion para los preview
        //$galeria1 = array();
        //$config = array();
        //foreach($recs as $rec){
            //$galeria1[] = asset('storage/uploads/'.$rec->rec_archivo);
            //$config[] = array('key'=>$rec->rec_codigo,'caption' => $rec->rec_archivo, 'url' => asset('gallery/image/'.$rec->rec_codigo.'/delete'));
        //}

        // Genero la vista ya armada
        $galeria = view('componentes.upload_image');

        // Retorno el componente
        return $galeria;
    }

}