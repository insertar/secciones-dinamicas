<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recurso extends Model
{
  public $table = "tbl_recursos";
  protected $fillable = ['rec_codigo','rec_codsec', 'rec_grupo', 'rec_tipo', 'rec_indice', 'rec_valor_simple','rec_valor_text'];
  protected $primaryKey = 'rec_codigo';

}
