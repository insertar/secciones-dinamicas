<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ToggleSwitchServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      \App::bind('toggle_switch', function()
      {
        return new \App\Components\ToggleSwitch;
      });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
