<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class UploadImageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('upload_image', function()
        {
            return new \App\Components\Upload_Image;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
