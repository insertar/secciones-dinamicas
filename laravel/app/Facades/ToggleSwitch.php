<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class ToggleSwitch extends Facade {

  protected static function getFacadeAccessor()
  {
    return 'toggle_switch';
  }
}