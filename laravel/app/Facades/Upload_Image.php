<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class Upload_Image extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'upload_image';
    }
}