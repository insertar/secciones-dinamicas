<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoRecurso extends Model
{
  public $table = "tbl_tipos_recursos";
  protected $fillable = ['trc_codigo','trc_nombre', 'trc_tipo', 'trc_activo'];
  protected $primaryKey = 'trc_codigo';
}
