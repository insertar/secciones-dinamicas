<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPublicacion extends Model
{
  public $table = "tbl_tipo_publicaciones";
  protected $fillable = ['pub_codigo','pub_nombre', 'pub_estado'];
  protected $primaryKey = 'pub_codigo';
}
